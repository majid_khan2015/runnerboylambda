package com.es.client.util;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * 
 * @author Majid
 *
 */
public class DBConnection {

	//qa
	
 	
	private static String userName = "qa_user";
	private static String password = "Quality@#$54";
	private static String url = "jdbc:mysql://cashcare-qa.c5cc4erpgekm.ap-south-1.rds.amazonaws.com:3306/earlysalary";
	private static String driver = "com.mysql.jdbc.Driver";
	
	
	//prod with RoUser
	
 /*
	private static String userName = "rouser";
	private static String password = "RU@Es#123";
	private static String url = "jdbc:mysql://es.c5cc4erpgekm.ap-south-1.rds.amazonaws.com:7011/rbmanager";
	private static String driver = "com.mysql.jdbc.Driver";	
  */
	
	//prod with Live user
	
/*	private static String userName = "live_user";
	private static String password = "Live@es1##$$";
	private static String url = "jdbc:mysql://es.c5cc4erpgekm.ap-south-1.rds.amazonaws.com:7011/earlysalary";
	private static String driver = "com.mysql.jdbc.Driver";
*/
	
/*	private static String userName = System.getenv("userName");
	private static String password = System.getenv("password");
	private static String url = System.getenv("url");
	private final static String driver = "com.mysql.jdbc.Driver";
*/
	
	public static Connection getConnection() {
		Connection conn = null;
		try {
			Class.forName(driver).newInstance();
			conn = DriverManager.getConnection(url, userName, password);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
}

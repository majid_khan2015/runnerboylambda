/**
 * 
 */
package com.es.client.dao;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.es.client.bussinesslogic.SendSmsService;
import com.es.client.model.GPSlocationModel;
import com.es.client.model.IdProofModel;
import com.es.client.model.ImageList;
import com.es.client.model.ResponseCodeModel;
import com.es.client.model.UploadImageRequest;
import com.es.client.util.DBConnection;
import com.google.gson.Gson;

/**
 * @author majidkhan
 */
public class DocUploadDaoImp implements DocUploadDaoInter {
	// private static String URL_PATH1 = "http://ops.earlysalary.in/doc";

	@Override
	public ResponseCodeModel updateDocImagePath(UploadImageRequest uploadImageRequestModel) {

		Connection connection = DBConnection.getConnection();

		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			connection.setAutoCommit(false);
			// String idProofName = "Signed NACH MANDATE";
			Integer subs_id = 0;
			Integer verificationStatus = 0;
			Long mobileNumber = 0L;
			String firstName = "";

			String queryTblSubscriber = "select * from earlysalary.tbl_subscribers where customer_code = ? ORDER BY subs_id DESC LIMIT 1";

			PreparedStatement preparedStatementTblSubs = connection.prepareStatement(queryTblSubscriber);
			preparedStatementTblSubs.setString(1, uploadImageRequestModel.getCustomerId());
			ResultSet resultSetTblSubs = preparedStatementTblSubs.executeQuery();

			if (resultSetTblSubs != null) {
				while (resultSetTblSubs.next()) {
					subs_id = resultSetTblSubs.getInt("subs_id");

					firstName = resultSetTblSubs.getString("first_name");
					mobileNumber = resultSetTblSubs.getLong("mobile_number");
					verificationStatus = resultSetTblSubs.getInt("verification_status");
					System.out.println("TblSubs::" + subs_id);
					System.out.println("TblSubs Verification Status::" + verificationStatus);
					System.out.println("TblSubs Customer Code::" + uploadImageRequestModel.getCustomerId());
				}
			}

			String queryAgencyCusMaster = "select id, status from rbmanager.agency_cust_master where cust_id = ? ORDER BY id DESC LIMIT 1";

			PreparedStatement preparedStatementAgencyCusMaster = connection.prepareStatement(queryAgencyCusMaster);
			preparedStatementAgencyCusMaster.setString(1, uploadImageRequestModel.getCustomerId());
			ResultSet resultSetAgencyCusMaster = preparedStatementAgencyCusMaster.executeQuery();
			resultSetAgencyCusMaster.next();
			System.out.println("agency_cust_master agency Id ::" + resultSetAgencyCusMaster.getInt("id"));

			if (resultSetAgencyCusMaster != null) {
				Integer agenyId = resultSetAgencyCusMaster.getInt("id");
				
				if (resultSetAgencyCusMaster.getInt("status") == 2003) {
					System.out.println("Upload Failed.. Invalid Agency code found:: " + resultSetAgencyCusMaster.getInt("status"));
					return new ResponseCodeModel("Failed to upload the document");
				}

				// while(resultSetAgencyCusMaster.next()) {
				if (uploadImageRequestModel.getImagePath().size() > 0) {
					for (int i = 0; i < uploadImageRequestModel.getImagePath().size(); i++) {
						int index = uploadImageRequestModel.getImagePath().get(i).getDocPath().lastIndexOf('.');

						String nameInIdProofCol;
						if (uploadImageRequestModel.getImagePath().get(i).getDocId().equals("32")) {
							nameInIdProofCol = System.getenv("idProofName");
						} else {
							nameInIdProofCol = System.getenv("idProofKycName");
						}

						IdProofModel idmodel = setImagedata(subs_id,
								uploadImageRequestModel.getImagePath().get(i).getDocId(),
								"/".concat(uploadImageRequestModel.getImagePath().get(i).getDocPath().replaceAll("\\\\",
										"")),
								uploadImageRequestModel.getImagePath().get(i).getDocPath().substring(index + 1),
								nameInIdProofCol, format.format(new Date()), uploadImageRequestModel.getFileSize(),
								new File(uploadImageRequestModel.getImagePath().get(i).getDocPath()).getName(),
								uploadImageRequestModel.getRb_id());

						System.out.println("agency status::" + resultSetAgencyCusMaster.getInt("status"));

						if (resultSetAgencyCusMaster.getInt("status") == 2016
								|| resultSetAgencyCusMaster.getInt("status") == 2010) {
							idmodel.setIs_active("Y");
						} else if (resultSetAgencyCusMaster.getInt("status") == 2008) {
							idmodel.setIs_active("P");
						}
						/*
						 * else { System.out.println("Upload Failed.. Invalid Agency code found:: "+
						 * resultSetAgencyCusMaster.getInt("status"));
						 * if(resultSetAgencyCusMaster.getInt("status") == 2003 &&
						 * uploadImageRequestModel.getImagePath().get(i).getDocId().equals("67") ||
						 * uploadImageRequestModel.getImagePath().get(i).getDocId().equals("71")) {
						 * System.out.println("Uploaded file for Kyc Data with doc id 67");
						 * insetIdModel(idmodel, connection); } return new ResponseCodeModel(
						 * "Upload Failed.. Invalid Agency code found - " +
						 * resultSetAgencyCusMaster.getInt("status")); }
						 */
						insetIdModel(idmodel, connection, uploadImageRequestModel.getGps_location());
					}

					// insetIdModel(idmodel, connection);
				

					String updateTblSubscriberQuery = "update earlysalary.tbl_subscribers set verification_status=? where subs_id="
							+ subs_id;
					PreparedStatement preparedStatementTblSubscriber = connection
							.prepareStatement(updateTblSubscriberQuery);

					if (verificationStatus == 3016 || verificationStatus == 8004) {
						System.out.println("No Need to change customer status ::" + verificationStatus);
					} else if (verificationStatus == 3028 || verificationStatus == 3029) {
						preparedStatementTblSubscriber.setInt(1, 3016);
						preparedStatementTblSubscriber.executeUpdate();

						// update customer status from Block(B) to
						// normal(N)
						String queryCustomerStatus = "select * from earlysalary.customer_status where customer_id = ? ORDER BY id DESC LIMIT 1";

						PreparedStatement preparedStatementCustomerStatus = connection
								.prepareStatement(queryCustomerStatus);
						preparedStatementCustomerStatus.setString(1, uploadImageRequestModel.getCustomerId());
						ResultSet resultSetCustomerStatus = preparedStatementCustomerStatus.executeQuery();

						String updateUpdateCustStatus = "update earlysalary.customer_status set status_code=?, status_desc = ?, "
								+ "updated_by=? where customer_id=" + uploadImageRequestModel.getCustomerId();

						if (resultSetCustomerStatus != null) {
							PreparedStatement preparedStatementUpdateCustStatus = connection
									.prepareStatement(updateUpdateCustStatus);

							while (resultSetCustomerStatus.next()) {
								// Update Table customer_status of earlysalary..

								preparedStatementUpdateCustStatus.setString(1, "N");
								preparedStatementUpdateCustStatus.setString(2, "Normal Account");
								preparedStatementUpdateCustStatus.setString(3,
										String.valueOf(uploadImageRequestModel.getRb_id()));
								preparedStatementUpdateCustStatus.executeUpdate();
								preparedStatementUpdateCustStatus.close();

								System.out.println("Customer Status Table Updated Succfully");
							}
						}
						preparedStatementTblSubscriber.close();
						preparedStatementCustomerStatus.close();

					} else {
						preparedStatementTblSubscriber.setInt(1, 8004);
						preparedStatementTblSubscriber.executeUpdate();
						preparedStatementTblSubscriber.close();

						// ------------update customer credit Details
						setCreditlimit(uploadImageRequestModel.getCustomerId(), subs_id, connection);
					}

					// UPDATE CUSTOMER PRODUCT DETAILS
					updateCustomerProductDetailsTable(uploadImageRequestModel.getCustomerId(), subs_id, connection);

					// Insert into tbl_sanction_loan_detail
					saveSanctionModel(uploadImageRequestModel.getCustomerId(), connection);

					// Update Agency Cust Master Table Status
					String queryUpdateAgencyCustMaster = "update rbmanager.agency_cust_master set status = ?, status_desc = ?,"
							+ "updated_on = ? where id = ?";

					PreparedStatement psUpdateAgencyCustMaster = connection
							.prepareStatement(queryUpdateAgencyCustMaster);
					psUpdateAgencyCustMaster.setInt(1, 2003);
					psUpdateAgencyCustMaster.setString(2, "Document Uploaded");
					psUpdateAgencyCustMaster.setString(3, format.format(new java.util.Date()));
					psUpdateAgencyCustMaster.setInt(4, agenyId);
					psUpdateAgencyCustMaster.executeUpdate();
					psUpdateAgencyCustMaster.close();
					System.out.println("RBManager agency_cust_master update Successfully");

					// Update tbl_emp_customer
					String queryUpdateTblEmpCustomer = "update rbmanager.tbl_emp_customer set status_code = ?, status_desc = ? where id =?";

					PreparedStatement psUpdateTblEmpCustomer = connection.prepareStatement(queryUpdateTblEmpCustomer);
					psUpdateTblEmpCustomer.setInt(1, 2003);
					psUpdateTblEmpCustomer.setString(2, "Document Uploaded");
					psUpdateTblEmpCustomer.setInt(3, uploadImageRequestModel.getPickup_id());
					psUpdateTblEmpCustomer.executeUpdate();
					psUpdateTblEmpCustomer.close();
					System.out.println("RBManager tbl_emp_customer update Successfully for PickUp Id::"
							+ uploadImageRequestModel.getPickup_id());

					// Call Send SMS
					new SendSmsService().senddocUploadMessage(mobileNumber, firstName);

					// Commit all db changes.
					connection.commit();
					System.out.println("Transaction Commited------>");
				} else {
					return new ResponseCodeModel("Please Try again later");
				}

				return new ResponseCodeModel("success");
				/*
				 * } else { return new ResponseCodeModel("Please Try again later"); }
				 */
				// }
			}
		} catch (Exception e) {
			try {
				if (connection != null) {
					connection.rollback();
				}
			} catch (SQLException e1) {
				return new ResponseCodeModel("Upload Failed..");
			}
			return new ResponseCodeModel("Upload Failed..");

		} finally {
			try {
				if (connection != null) {
					connection.close();
				}
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return new ResponseCodeModel("success");
	}

	private void setCreditlimit(String customerId, Integer subs_id, Connection connection) {
		try {
			System.out.println(">>>>> Update customer_credit_details <<<<<<<");

			String queryFetchCreditDet = "select cash_limit, credit_limit, cust_credit_id from earlysalary.customer_credit_details where customer_id = ? and"
					+ " subs_id = ? ORDER BY cust_credit_id DESC LIMIT 1";

			PreparedStatement psFetchCreditDet = connection.prepareStatement(queryFetchCreditDet);
			psFetchCreditDet.setString(1, customerId);
			psFetchCreditDet.setInt(2, subs_id);
			ResultSet resultSet = psFetchCreditDet.executeQuery();

			if (resultSet != null) {
				if (resultSet.next()) {
					String queryUpdateCreditDet = "update earlysalary.customer_credit_details set cash_balance = ?, credit_balance = ?, "
							+ "display_cash_balance = ?, display_credit_balance = ?, total_cash_usage = ?, total_credit_usage = ?, "
							+ "eligible_for_loan = ? where cust_credit_id = ?";

					PreparedStatement psUpdateCreditDet = connection.prepareStatement(queryUpdateCreditDet);
					System.out.println(
							">>>>> cust_credit_id from customer_credit_details::" + resultSet.getInt("cust_credit_id"));
					psUpdateCreditDet.setDouble(1, resultSet.getDouble("cash_limit"));
					psUpdateCreditDet.setDouble(2, resultSet.getDouble("credit_limit"));
					psUpdateCreditDet.setDouble(3, 0.0);
					psUpdateCreditDet.setDouble(4, 0.0);
					psUpdateCreditDet.setDouble(5, 0.0);
					psUpdateCreditDet.setDouble(6, 0.0);
					psUpdateCreditDet.setBoolean(7, true);
					psUpdateCreditDet.setInt(8, resultSet.getInt("cust_credit_id"));

					int state = psUpdateCreditDet.executeUpdate();
					psUpdateCreditDet.close();

					System.out.println("customer_credit_details updated Succfully::" + state);

				}
			}
			psFetchCreditDet.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void updateCustomerProductDetailsTable(String customerId, Integer subs_id, Connection connection) {
		try {
			String queryUpdateProdDetails = "update earlysalary.customer_product_detail set is_eligible = 'Y' where customer_id=?"
					+ "and subs_id =? and m_prod_id in(23,3,25) and is_active = 'Y' and is_eligible = 'N'";

			PreparedStatement psUpdateCustomerProdDetails = connection.prepareStatement(queryUpdateProdDetails);
			psUpdateCustomerProdDetails.setString(1, customerId);
			psUpdateCustomerProdDetails.setInt(2, subs_id);
			psUpdateCustomerProdDetails.executeUpdate();
			psUpdateCustomerProdDetails.close();

			System.out.println("customer_product_detail updated Succfully");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveSanctionModel(String customerId, Connection connection) {
		try {
			String queryFetchData = "select sanction_loan_id from earlysalary.tbl_sanction_loan_detail where customer_id = ? and "
					+ "sanction_status = 'PENDING-DISB' ORDER BY sanction_loan_id DESC LIMIT 1";

			PreparedStatement preparedStatement = connection.prepareStatement(queryFetchData);
			preparedStatement.setString(1, customerId);
			ResultSet resultSet = preparedStatement.executeQuery();

			if (resultSet != null) {
				if (resultSet.next()) {

					String queryInsertSanctionLoanDetail = "update earlysalary.tbl_sanction_loan_detail set sanction_status = ? where sanction_loan_id = ?";

					PreparedStatement psUpdateSanctionLoanDetail = connection
							.prepareStatement(queryInsertSanctionLoanDetail);
					System.out.println(">>>>> sanction_loan_id from tbl_sanction_loan_detail::"
							+ resultSet.getString("sanction_loan_id"));
					psUpdateSanctionLoanDetail.setString(1, "APPROVED");
					psUpdateSanctionLoanDetail.setString(2, resultSet.getString("sanction_loan_id"));
					psUpdateSanctionLoanDetail.executeUpdate();
					psUpdateSanctionLoanDetail.close();
					preparedStatement.close();

					System.out.println("tbl_sanction_loan_detail upadte Succfully");
				} else {
					System.out.println("tbl_sanction_loan_detail is not available for customer id " + customerId);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	private void insetIdModel(IdProofModel idmodel, Connection connection, List<GPSlocationModel> gpsLocation) {
		try {
			
			String insertIdProofQuery = "insert into earlysalary.tbl_subs_idproof_details (tbl_subscribers_subs_id, "
					+ "tbl_id_proof_proof_id, image_path, image_extension, image_size, image_name, create_date, "
					+ "is_active, verified_by, name_in_idproof, address_in_id) values (?,?,?,?,?,?,?,?,?,?,?)";

			PreparedStatement preparedStatementIdProof = connection.prepareStatement(insertIdProofQuery);
			preparedStatementIdProof.setInt(1, idmodel.getSubs_id());
			preparedStatementIdProof.setInt(2, idmodel.getPancard_proof_id());
			preparedStatementIdProof.setString(3, idmodel.getImage_path());
			preparedStatementIdProof.setString(4, idmodel.getImage_extension());
			preparedStatementIdProof.setLong(5, idmodel.getImage_size());
			preparedStatementIdProof.setString(6, idmodel.getImage_name());
			preparedStatementIdProof.setString(7, idmodel.getCreate_date());
			preparedStatementIdProof.setString(8, idmodel.getIs_active());
			preparedStatementIdProof.setInt(9, idmodel.getVerified_by());
			preparedStatementIdProof.setString(10, idmodel.getName_in_idproof());
			
			if (gpsLocation != null && gpsLocation.get(0) != null) {
				preparedStatementIdProof.setString(11, gpsLocation.get(0).getGps_latitude()+","+gpsLocation.get(0).getGps_longitude());
			} else {
				preparedStatementIdProof.setString(11, "");
			}

			preparedStatementIdProof.executeUpdate();
			preparedStatementIdProof.close();

			System.out.println(
					"Insert Successfully in tbl_subs_idproof_details against subs id::" + idmodel.getSubs_id());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private IdProofModel setImagedata(Integer subs_id, String docIdNumber, String docPath, String docExtension,
			String idProofName, String currentDate, Long fileSize, String fileName, int rb_id) {
		// TODO Auto-generated method stub
		System.out.println("subsID::" + subs_id);
		System.out.println("docId::" + docIdNumber);
		System.out.println("docPath::" + docPath);
		System.out.println("docExtension::" + docExtension);
		System.out.println("idProofName::" + idProofName);
		System.out.println("currentDate::" + currentDate);
		System.out.println("fileSize::" + fileSize);
		System.out.println("fileName::" + fileName);
		System.out.println("rb_id::" + rb_id);

		IdProofModel idProofModel = new IdProofModel();
		try {
			idProofModel.setSubs_id(subs_id);
			idProofModel.setPancard_proof_id(Integer.parseInt(docIdNumber));
			idProofModel.setImage_path(docPath);
			idProofModel.setImage_extension(docExtension);
			idProofModel.setImage_size(fileSize);
			idProofModel.setName_in_idproof(fileName);
			idProofModel.setCreate_date(currentDate);
			idProofModel.setImage_name(fileName);
			idProofModel.setVerified_by(rb_id);
			idProofModel.setName_in_idproof(idProofName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return idProofModel;
	}

	public static void main(String[] args) {

		List<ImageList> list = new ArrayList<>();
		List<GPSlocationModel> gps_location=new ArrayList<>();
		
		ImageList imageList = new ImageList();
		imageList.setDocId("32");
		imageList.setDocPath("32/1410000154/16012019/1547642843419.jpg");
		list.add(imageList);

		GPSlocationModel gps = new GPSlocationModel();
		gps.setGps_latitude("18.5672353");
		gps.setGps_longitude("73.9198386");
		gps.setGps_houseno("The Chambers");
		gps.setGps_street("Viman Nagar");
		gps.setGps_state("Maharashtra");
		gps.setGps_city("Pune");
		gps.setGps_country("India");
		gps.setGps_pinCode("411014");
		gps_location.add(gps);
		
		UploadImageRequest runnerBoyInfoModel = new UploadImageRequest();
		runnerBoyInfoModel.setCustomerId("1410000154");
		runnerBoyInfoModel.setRb_id(1);
		runnerBoyInfoModel.setPickup_id(15);
		runnerBoyInfoModel.setFileSize(1024l);
		runnerBoyInfoModel.setImagePath(list);
		runnerBoyInfoModel.setGps_location(gps_location);
		
		//System.out.println("Upload Doc Payload::" + new Gson().toJson(runnerBoyInfoModel));
		new DocUploadDaoImp().updateDocImagePath(runnerBoyInfoModel);
	}
}

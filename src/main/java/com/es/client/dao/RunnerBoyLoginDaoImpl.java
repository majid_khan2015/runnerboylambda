package com.es.client.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.es.client.bussinesslogic.MD5Convert;
import com.es.client.model.RBCustomerResponseVO;
import com.es.client.model.RbLoginResponseVo;
import com.es.client.model.ResponseCodeModel;
import com.es.client.model.RunnerBoyInfoModel;
import com.es.client.model.RunnerBoyResponseVO;
import com.es.client.model.UpdateUrlDescVO;
import com.es.client.util.DBConnection;
import com.google.gson.Gson;
import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

public class RunnerBoyLoginDaoImpl implements RunnerBoyLoginDaoInter {

	//private static String URL_PATH = "http://ops.earlysalary.in/doc/32/";
	//private static String URL_PATH1 = "http://ops.earlysalary.in/doc";

	public ResponseCodeModel runnerBoy(RunnerBoyInfoModel runnerBoyInfoModel) {
		RbLoginResponseVo rbLoginResponseVo = null;

		try (Connection connection = DBConnection.getConnection();) {
			if (runnerBoyInfoModel != null) {

				rbLoginResponseVo = new RbLoginResponseVo();
				Boolean isRunnerBoyAvail = false;

				// MD5Convert conversion = new MD5Convert();
				String password = new MD5Convert().md5Conversion(runnerBoyInfoModel.getPassword());

				String query = "Select id,first_name,last_name,phone,email from rbmanager.tbl_emp_login where rb_username = ? and password = ? and active = 1 "
						+ "ORDER BY id DESC LIMIT 1";

				PreparedStatement preparedStatement = connection.prepareStatement(query);
				preparedStatement.setString(1, runnerBoyInfoModel.getUsername());
				preparedStatement.setString(2, password);
				ResultSet resultSet = preparedStatement.executeQuery();

				if (resultSet != null) {
					while (resultSet.next()) {
						isRunnerBoyAvail = true;
						rbLoginResponseVo.setRb_id(resultSet.getInt("id"));
						rbLoginResponseVo.setFirstname(resultSet.getString("first_name"));
						rbLoginResponseVo.setLastname(resultSet.getString("last_name"));
						rbLoginResponseVo.setMobile_number(resultSet.getLong("phone"));
						rbLoginResponseVo.setEmail(resultSet.getString("email"));

						System.out.println("RB_ID::::::" + resultSet.getInt("id"));
					}

					preparedStatement.close();
					
					if(!isRunnerBoyAvail) {
						return new ResponseCodeModel("invalid username and password");
					}

					// insert logmaster model
					query = "insert into rbmanager.rb_log_master (module_name,rb_id,app_id,app_version,log_date,log_time,gcm_id,latitude,longitude) "
							+ "values (?,?,?,?,?,?,?,?,?)";
					PreparedStatement preparedStatement2 = connection.prepareStatement(query);
					preparedStatement2.setString(1, "Login");
					System.out.println("RBID:::" + rbLoginResponseVo.getRb_id());
					preparedStatement2.setInt(2, rbLoginResponseVo.getRb_id());
					preparedStatement2.setInt(3, runnerBoyInfoModel.getApp_id());
					preparedStatement2.setString(4, runnerBoyInfoModel.getApp_version());
					preparedStatement2.setDate(5, new java.sql.Date(new java.util.Date().getTime()));
					preparedStatement2.setTime(6, new java.sql.Time(new java.util.Date().getTime()));
					preparedStatement2.setString(7, runnerBoyInfoModel.getGcm_id());
					if (runnerBoyInfoModel.getGps_location() != null
							&& runnerBoyInfoModel.getGps_location().get(0) != null) {
						preparedStatement2.setString(8, runnerBoyInfoModel.getGps_location().get(0).getGps_latitude());
						preparedStatement2.setString(9, runnerBoyInfoModel.getGps_location().get(0).getGps_longitude());
					} else {
						preparedStatement2.setString(8, "");
						preparedStatement2.setString(9, "");
					}

					preparedStatement2.executeUpdate();
					preparedStatement2.close();

				}
			} else {
				return new ResponseCodeModel("Invalid username and password");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseCodeModel("Internal Server Error");
		}

		System.out.println("Login Response::" + new Gson().toJson(rbLoginResponseVo));
		return new ResponseCodeModel(rbLoginResponseVo);
	}

	public static void main(String[] args) {
		RunnerBoyInfoModel runnerBoyInfoModel = new RunnerBoyInfoModel();
		runnerBoyInfoModel.setUsername("pankaj");
		runnerBoyInfoModel.setPassword("password");

		// runnerBoyInfoModel.setUsername("sanket");
		// runnerBoyInfoModel.setPassword("1234");

		runnerBoyInfoModel.setRb_id(1);
		runnerBoyInfoModel.setApp_id(13);
		runnerBoyInfoModel.setApp_version("1.0.3");
		// runnerBoyInfoModel.setGcm_id("APA91bEtcRFrSRTK3luOR1ZloSZQksCdi6AhlJrO9b7nhHSQmX8Nm0Jz81xK_6iGJBlv_XsP4cmy8vIIBqAXj5E1NVOOyQnZ3FUEtq4tpxTRW9aR6nON6MdorbSUKmdpNmWX1iYsGn-q");
		// List<GPSlocationModel> gps_location = new ArrayList<>();
		// GPSlocationModel model = new GPSlocationModel();
		// model.setGps_latitude("18.5691306");
		// model.setGps_longitude("73.9082878");
		// gps_location.add(model);
		// runnerBoyInfoModel.setGps_location(gps_location);
		// runnerBoyInfoModel.setRb_id(452);
		// runnerBoyInfoModel.setApp_id(13);
		// new RunnerBoyLoginDaoImpl().getCustomerList(runnerBoyInfoModel);
		System.out.println("Login Request::" + new Gson().toJson(runnerBoyInfoModel));

		 System.out.println("Login Response::"+new RunnerBoyLoginDaoImpl().runnerBoy(runnerBoyInfoModel));
		//System.out.println(new Gson().toJson(new RunnerBoyLoginDaoImpl().getCustomerList(runnerBoyInfoModel)));

	}

	@Override
	public ResponseCodeModel getCustomerList(RunnerBoyInfoModel runnerBoyInfoModel) {
		int subs_id = 0;
		RunnerBoyResponseVO runnerBoyResponseVO = null;
		List<RunnerBoyResponseVO> runnerBoyResponseVoList = new ArrayList<RunnerBoyResponseVO>();
		UpdateUrlDescVO updateUrl = null;
		RBCustomerResponseVO rbCustomerMainResponseVO = new RBCustomerResponseVO();
		// TODO Auto-generated method stub
		long startTime = System.currentTimeMillis();
		try (Connection connection = DBConnection.getConnection();) {

			if (runnerBoyInfoModel.getRb_id() != null) {

				String query = "Select * from rbmanager.tbl_emp_login where id = ?";

				PreparedStatement preparedStatement = connection.prepareStatement(query);
				preparedStatement.setInt(1, runnerBoyInfoModel.getRb_id());
				ResultSet resultSet = preparedStatement.executeQuery();
				boolean isRbIdfound = false;

				while (resultSet.next()) {
					isRbIdfound = true;

					if (resultSet.getInt("active") == 0) {
						rbCustomerMainResponseVO.setRb_id(0);
						return new ResponseCodeModel("RunnerBoy is not active");
					} else {
						String appMasterQuery = "select app_location_url,update_url,app_ver_code,app_ver_desc,force_update from earlysalary.app_master where app_id = ?";
						PreparedStatement preparedStatementAppMaster = connection.prepareStatement(appMasterQuery);
						preparedStatementAppMaster.setInt(1, runnerBoyInfoModel.getApp_id());
						ResultSet resultSetAppData = preparedStatementAppMaster.executeQuery();
						if (resultSetAppData != null) {
							while (resultSetAppData.next()) {
								updateUrl = new UpdateUrlDescVO();
								updateUrl.setApp_location_url(resultSetAppData.getString(1));
								updateUrl.setApp_update_url(resultSetAppData.getString(2));
								updateUrl.setApp_version(resultSetAppData.getString(3));
								updateUrl.setApp_version_desc(resultSetAppData.getString(4));
								if (resultSetAppData.getInt(5) == 1) {
									updateUrl.setUpdate_flag(true);
									rbCustomerMainResponseVO.setUpdateUrlDescVO(updateUrl);
									// return new ResponseCodeModel(rbCustomerResponseVO);
								} else {
									updateUrl.setUpdate_flag(false);
								}
								rbCustomerMainResponseVO.setUpdateUrlDescVO(updateUrl);
							}
						}
						preparedStatementAppMaster.close();

						// Get All Data from DB for client from tbl_emp_customer.
						String queryFetchData = "Select rb_id,status_code,customer_id,id from rbmanager.tbl_emp_customer where rb_id=? "
								+ "and customer_id <>0 and status_code=? order by created_on desc limit 200";
						PreparedStatement preparedStatementFethData = connection.prepareStatement(queryFetchData);
						preparedStatementFethData.setInt(1, runnerBoyInfoModel.getRb_id());
						preparedStatementFethData.setInt(2, 2001);
						ResultSet resultSetFetchData = preparedStatementFethData.executeQuery(); 

						while (resultSetFetchData.next()) {

							// Fetch data from tbl Subscribers Table
							String queryFetchTbSubscriber = "SELECT subs_id,mobile_number,first_name,last_name,email_id,date_of_birth FROM "
									+ "earlysalary.tbl_subscribers WHERE customer_code = ? and verification_status in (3016,3020,3022,8004,3028,3029)";

							PreparedStatement psFetchTblSubs = connection.prepareStatement(queryFetchTbSubscriber);
							psFetchTblSubs.setLong(1, resultSetFetchData.getLong("customer_id"));
							ResultSet resultSetFetchTblSubs = psFetchTblSubs.executeQuery();

							runnerBoyResponseVO = new RunnerBoyResponseVO();

							while (resultSetFetchTblSubs.next()) {
								subs_id = resultSetFetchTblSubs.getInt("subs_id");

								runnerBoyResponseVO.setMobile_number(
										String.valueOf(resultSetFetchTblSubs.getString("mobile_number")));
								runnerBoyResponseVO.setFirstname(resultSetFetchTblSubs.getString("first_name"));
								runnerBoyResponseVO.setLastname(resultSetFetchTblSubs.getString("last_name"));
								runnerBoyResponseVO.setEmail(resultSetFetchTblSubs.getString("email_id"));
								runnerBoyResponseVO.setDob(resultSetFetchTblSubs.getString("date_of_birth"));
							}

							resultSetFetchTblSubs.close();

							// Fetch data from tbl delivery address table
							String queryFetchTblDeliveryAdd = "select address1,address2,selected_date,selected_time,city,state,landmark,pincode from earlysalary.tbl_delivery_address "
									+ "where customer_id = ? and status='Y' order by id desc limit 1";

							PreparedStatement psFetchDeliveryAdd = connection
									.prepareStatement(queryFetchTblDeliveryAdd);
							psFetchDeliveryAdd.setLong(1, resultSetFetchData.getLong("customer_id"));
							ResultSet resultSetDeliveryAdd = psFetchDeliveryAdd.executeQuery();

							while (resultSetDeliveryAdd.next()) {
								runnerBoyResponseVO.setAddress(resultSetDeliveryAdd.getString("address1"));
								runnerBoyResponseVO.setAddress2(resultSetDeliveryAdd.getString("address2"));
								runnerBoyResponseVO.setSelected_date(resultSetDeliveryAdd.getString("selected_date"));
								runnerBoyResponseVO.setSelected_time(resultSetDeliveryAdd.getString("selected_time"));
								runnerBoyResponseVO.setLandmark(resultSetDeliveryAdd.getString("landmark"));
								runnerBoyResponseVO.setCity(resultSetDeliveryAdd.getString("city"));
								runnerBoyResponseVO.setState(resultSetDeliveryAdd.getString("state"));
								runnerBoyResponseVO.setPincode(resultSetDeliveryAdd.getString("pincode"));
							}
							resultSetDeliveryAdd.close();

							// Fetch data from subs id proof details table
							String queryFetchTblSubsIdProofDetails = "select Id_number from earlysalary.tbl_subs_idproof_details where tbl_subscribers_subs_id = ? "
									+ "and tbl_id_proof_proof_id =8 and id_number is not null order by subs_proof_id desc limit 1";

							PreparedStatement psFetchSubsIdProofDet = connection
									.prepareStatement(queryFetchTblSubsIdProofDetails);
							psFetchSubsIdProofDet.setInt(1, subs_id);
							ResultSet resultSetSubsIdProofDet = psFetchSubsIdProofDet.executeQuery();

							while (resultSetSubsIdProofDet.next()) {
								runnerBoyResponseVO.setPancard(resultSetSubsIdProofDet.getString("Id_number"));
							}
							resultSetSubsIdProofDet.close();

							// Fetch data from subs id proof details table get selfie url
							String queryFetchTblSubsIdProofDetails2 = "select image_path from earlysalary.tbl_subs_idproof_details where tbl_subscribers_subs_id = ? "
									+ "and tbl_id_proof_proof_id = 9 order by subs_proof_id desc limit 1";

							PreparedStatement psFetchSubsIdProofDet2 = connection
									.prepareStatement(queryFetchTblSubsIdProofDetails2);
							psFetchSubsIdProofDet2.setInt(1, subs_id);
							ResultSet resultSetSubsIdProofDet2 = psFetchSubsIdProofDet2.executeQuery();

							while (resultSetSubsIdProofDet2.next()) {
								if (resultSetSubsIdProofDet2.getString("image_path")
										.contains("https://socialworth.in/")) {
									runnerBoyResponseVO.setSelfie_url(resultSetSubsIdProofDet2.getString("image_path"));
								} else if (resultSetSubsIdProofDet2.getString("image_path").contains("/disk/mount/")) {
									runnerBoyResponseVO.setSelfie_url(System.getenv("URL_PATH").concat(resultSetSubsIdProofDet2
											.getString("image_path").replace("/disk/mount/doc", "")));
								} else if (resultSetSubsIdProofDet2.getString("image_path")
										.contains("/opt/apps/es/doc")) {
									runnerBoyResponseVO.setSelfie_url(System.getenv("URL_PATH").concat(resultSetSubsIdProofDet2
											.getString("image_path").replace("/opt/apps/es/doc", "")));
								} else {
									runnerBoyResponseVO.setSelfie_url(
											System.getenv("URL_PATH1").concat(resultSetSubsIdProofDet2.getString("image_path")));
								}
							}
							
							resultSetSubsIdProofDet2.close();
							
							//fetch data from agency cust master
							String fetchDataAgencyCustMaster = "select carrier_id from rbmanager.agency_cust_master where cust_id = ? ORDER BY id DESC LIMIT 1";
							
							PreparedStatement psFetchAgencyData = connection.prepareStatement(fetchDataAgencyCustMaster);
							psFetchAgencyData.setLong(1, resultSetFetchData.getLong("customer_id"));
							ResultSet resultSetAgencyCust = psFetchAgencyData.executeQuery();
							if(resultSetAgencyCust.next()) {
								runnerBoyResponseVO.setCarrierId(resultSetAgencyCust.getInt("carrier_id"));
							}
							resultSetAgencyCust.close();
							
							runnerBoyResponseVO
									.setCustomer_id(String.valueOf(resultSetFetchData.getString("customer_id")));
							runnerBoyResponseVO.setPickup_id(String.valueOf(resultSetFetchData.getInt("id")));
							runnerBoyResponseVO.setStatus(String.valueOf(resultSetFetchData.getInt(2)));

							runnerBoyResponseVoList.add(runnerBoyResponseVO);
						}
						
						rbCustomerMainResponseVO.setRb_id(runnerBoyInfoModel.getRb_id());
						rbCustomerMainResponseVO.setActive(String.valueOf(resultSet.getInt("active")));
						rbCustomerMainResponseVO.setRunnerBoyResponseVO(runnerBoyResponseVoList);
						
						resultSetFetchData.close();
					}

				}
				if (!isRbIdfound)
					return new ResponseCodeModel("Rb Id Not Found");

			} else {
				return new ResponseCodeModel("Rb Id Null");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseCodeModel("Internal Server Error");
		}
		long endTime = System.currentTimeMillis();
		System.out.println(" Fetch Cust Total Time Taken: " + (endTime - startTime) / 1000 + " seconds");
		//System.out.println("runnerBoyResponseVO:::" + new Gson().toJson(rbCustomerMainResponseVO));
		
		return new ResponseCodeModel(rbCustomerMainResponseVO);
	}
}

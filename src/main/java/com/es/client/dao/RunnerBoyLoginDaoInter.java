package com.es.client.dao;

import com.es.client.model.ResponseCodeModel;
import com.es.client.model.RunnerBoyInfoModel;

/**
 * 
 * @author majidkhan
 *
 */
public interface RunnerBoyLoginDaoInter {
	public ResponseCodeModel runnerBoy(RunnerBoyInfoModel runnerBoyInfoModel);
	public ResponseCodeModel getCustomerList(RunnerBoyInfoModel runnerBoyInfoModel); 
}

/**
 * 
 */
package com.es.client.dao;

import com.es.client.model.ResponseCodeModel;
import com.es.client.model.UploadImageRequest;

/**
 * @author majidkhan
 *
 */
public interface DocUploadDaoInter {
	public ResponseCodeModel updateDocImagePath(UploadImageRequest uploadImageRequestModel);
}

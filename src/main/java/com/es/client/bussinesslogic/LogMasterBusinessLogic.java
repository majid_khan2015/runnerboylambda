/**
 * 
 */
package com.es.client.bussinesslogic;
 

import com.es.client.model.LogMasterModel;

/**
 * @author majidkhan
 *
 */
public class LogMasterBusinessLogic {
	public String logInsert(LogMasterModel logmastermodel) {
		try {
			LogMasterModel logMasterModel = new LogMasterModel();
			
			logmastermodel.setModule_name(logMasterModel.getModule_name());
			logmastermodel.setCustomer_code(logmastermodel.getCustomer_code());
			logmastermodel.setSubs_id(logmastermodel.getSubs_id());
			logmastermodel.setPickup_id(logmastermodel.getPickup_id());
			logmastermodel.setRb_id(logmastermodel.getRb_id());
			logmastermodel.setApp_id(logmastermodel.getApp_id());
			logmastermodel.setApp_version(logmastermodel.getApp_version());
			logmastermodel.setLog_date(logmastermodel.getLog_date());
			logmastermodel.setLog_time(logmastermodel.getLog_time());
			logmastermodel.setUser_agent(logmastermodel.getUser_agent());
			logmastermodel.setGcm_id(logmastermodel.getGcm_id());
			logmastermodel.setLatitude(logmastermodel.getLatitude());
			logmastermodel.setLongitude(logmastermodel.getLongitude());
			logmastermodel.setRemote_ip(logmastermodel.getRemote_ip());
			logmastermodel.setStatus_code(logmastermodel.getStatus_code());
			logmastermodel.setStatus_desc(logmastermodel.getStatus_desc());
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;

	}
}

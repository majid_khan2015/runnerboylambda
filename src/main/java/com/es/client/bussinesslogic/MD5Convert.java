/**
 * 
 */
package com.es.client.bussinesslogic;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
 

/**
 * @author majidkhan
 *
 */
public class MD5Convert {
	public  String md5Conversion(String password) {
		  try {
	            MessageDigest md = MessageDigest.getInstance("MD5");
	            byte[] messageDigest = md.digest(password.getBytes());
	            BigInteger number = new BigInteger(1, messageDigest);
	            String hashtext = number.toString(16);
	            // Now we need to zero pad it if you actually want the full 32 chars.
	            while (hashtext.length() < 32) {
	                hashtext = "0" + hashtext;
	            } 
	           return hashtext;
	        }
	        catch (NoSuchAlgorithmException e) { 
	            throw new RuntimeException(e);
	        }

		//return null;
	}
}

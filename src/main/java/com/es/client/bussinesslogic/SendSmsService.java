/**
 * 
 */
package com.es.client.bussinesslogic;

import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;



/**
 * @author majidkhan
 *
 */
public class SendSmsService {
	
	private static final String msgTemplates = System.getenv("msg_template");
	private static final String otpSendApi = System.getenv("otpSendAPI");
	
	//private static final String msgTemplates = "Dear XXX, We have received the Loan Agreement signed by you for Early Salary. In case this wasn't initiated by you, please reach us on 08030077553.";
	//private static final String otpSendApi = "https://socialworth.in/nocall?To=+91";
	
	public Boolean senddocUploadMessage(Long mobileNumber, String firstName) {
		// TODO Auto-generated method stub
		 try {
			 System.out.println("senddocUploadMessage Called Mobile -->>>>"+mobileNumber);
			 
			 String msg_tamplate = msgTemplates;
				msg_tamplate = msg_tamplate.replace("XXX", firstName);
				URL url = new URL(otpSendApi + mobileNumber + "&Text=" + URLEncoder.encode(msg_tamplate, "UTF-8"));
				HttpURLConnection connection = (HttpURLConnection) url.openConnection();
				System.out.println("OTP Url -->>>>"+url.toString()); 
				connection.setDoOutput(true);
				connection.setConnectTimeout(4000);
				connection.setReadTimeout(4000);
				System.out.println("Msg Sent Succesfully -->>>>"+connection.getResponseCode());
				return true;
		 }
		 catch(Exception e) {
			 System.out.println("Exception occure while send SMS "+e);
		 }
		return true;
	}


}

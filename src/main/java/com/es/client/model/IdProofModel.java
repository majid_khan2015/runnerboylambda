package com.es.client.model;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class IdProofModel {
	private Integer subs_proof_id; 
	private String Id_number; 
	private Integer subs_id; 
	private Integer pancard_proof_id;
	private String image_path; 
	private String image_extension; 
	private Long image_size; 
	private String image_name; 
	private String verify_status; 
	private String create_date; 
	private String name_in_idproof; 
	private Date valid_till_date; 
	private String password; 
	private Integer verified_by; 
	private String address_in_id; 
	private String is_active;
}

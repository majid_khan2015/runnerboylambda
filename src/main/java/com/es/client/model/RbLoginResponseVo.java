package com.es.client.model;

import lombok.Data;

@Data
public class RbLoginResponseVo {
	private Integer rb_id;
	private String last_login;
	private String firstname;
	private String lastname;
	private Long mobile_number;
	private String email;
	private String activeflag;
}

/**
 * 
 */
package com.es.client.model;

import java.util.Date;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class LogMasterModel {
	private Integer log_id;
	private String app_version;
	private Integer app_id;
	private Date log_date;
	private Date log_time;
	private String user_agent;
	private String gcm_id;
	private String latitude;
	private String longitude;
	private String cell_location;
	private String remote_ip;
	private Integer rb_id;
	private Integer status_code;
	private String status_desc;
	private String module_name;
	private Long customer_code;
	private Integer subs_id;
	private Integer pickup_id;
}

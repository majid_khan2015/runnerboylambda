package com.es.client.model;

import lombok.Data;

@Data
public class GPSlocationModel {
	private String gps_latitude;
	private String gps_longitude;
	private String gps_country;
	private String gps_state;
	private String gps_city;
	private String gps_street;
	private String gps_houseno;
	private String gps_pinCode;
}

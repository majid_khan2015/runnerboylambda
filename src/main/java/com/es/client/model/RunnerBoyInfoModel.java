package com.es.client.model;

import java.util.List;

import lombok.Data;

@Data
public class RunnerBoyInfoModel {
	private String username;
	private String password;
	private List<GPSlocationModel> gps_location;
	private Integer app_id;
	private String app_version;
	private String gcm_id;
	private Integer rb_id;
}

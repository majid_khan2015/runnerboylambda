package com.es.client.model;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class ImageList {
	private String docId;
	private String docPath;
}

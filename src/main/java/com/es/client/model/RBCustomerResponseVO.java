package com.es.client.model;

import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class RBCustomerResponseVO {
	private List<RunnerBoyResponseVO> runnerBoyResponseVO;
	private UpdateUrlDescVO updateUrlDescVO;
	private Integer rb_id=0;
	private Date last_login;
	private String active="";
}

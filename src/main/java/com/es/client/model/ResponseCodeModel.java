package com.es.client.model;
 
import lombok.Data;

@Data
public class ResponseCodeModel {
	private RbLoginResponseVo rbLoginResponse_vo;
	private RBCustomerResponseVO rBCustomerResponse_vo;
	private Boolean status;
	private Integer statusCode;
	private String message;

	public RBCustomerResponseVO getrBCustomerResponse_vo() {
		return rBCustomerResponse_vo;
	}

	public void setrBCustomerResponse_vo(RBCustomerResponseVO rBCustomerResponse_vo) {
		this.rBCustomerResponse_vo = rBCustomerResponse_vo;
	}

	public RbLoginResponseVo getRbLoginResponse_vo() {
		return rbLoginResponse_vo;
	}

	public void setRbLoginResponse_vo(RbLoginResponseVo rbLoginResponse_vo) {
		this.rbLoginResponse_vo = rbLoginResponse_vo;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public ResponseCodeModel(RBCustomerResponseVO rBCustomerResponseVO) {
		this.rBCustomerResponse_vo = rBCustomerResponseVO;

	}

	public ResponseCodeModel(RbLoginResponseVo rbLoginResponseVo) {
		this.rbLoginResponse_vo = rbLoginResponseVo;

	}

	public ResponseCodeModel(String message, Integer statusCode) {
		this.message = message;
		this.statusCode = statusCode;
	}

	public ResponseCodeModel(String message) {
		this.message = message;
	}

}

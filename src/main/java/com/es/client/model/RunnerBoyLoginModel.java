package com.es.client.model;

import java.util.Date;

import lombok.Data;

@Data
public class RunnerBoyLoginModel {
	private int id;
	private int user_id;
	private String username;
	private String rb_username;
	private String password;
	private String email;
	private Date last_login;
	private int active;
	private String first_name;
	private String last_name;
	private Long phone;
	private String address;
	private String area_alloted;
	private String city;
	private String rb_image;
}

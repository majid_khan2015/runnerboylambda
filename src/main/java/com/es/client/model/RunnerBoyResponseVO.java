package com.es.client.model;

import lombok.Data;

@Data
public class RunnerBoyResponseVO {
	private String customer_id = "";
	private String firstname = "";
	private String lastname = "";
	private String email = "";
	private String dob = "";
	private String mobile_number;
	private String Selfie_url = "";
	private String pancard = "";
	private String delivery_address = "";
	private String address = "";
	private String address2 = "";
	private String landmark = "";
	private String city = "";
	private String state = "";
	private String pincode = "";
	private String selected_date = "";
	private String selected_time = "";
	private String status = "";
	private String pickup_id = "";
	private Integer carrierId = null;
}

/**
 * 
 */
package com.es.client.model;

import java.util.List;

import lombok.Data;

/**
 * @author majidkhan
 *
 */

@Data
public class UploadImageRequest {
	private String customerId;
	private int rb_id;
	private int pickup_id;
	private Long fileSize;
	private List<ImageList> imagePath;
	private List<GPSlocationModel> gps_location;
}

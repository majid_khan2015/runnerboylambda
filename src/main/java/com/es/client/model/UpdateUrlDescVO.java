package com.es.client.model;

import lombok.Data;

@Data
public class UpdateUrlDescVO {
	private String app_version;
	private String app_version_desc;
	private String app_location_url;
	private String app_update_url;
	private Boolean update_flag;
}

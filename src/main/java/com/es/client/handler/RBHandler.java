package com.es.client.handler;

import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.es.client.dao.DocUploadDaoImp;
import com.es.client.dao.DocUploadDaoInter;
import com.es.client.dao.RunnerBoyLoginDaoImpl;
import com.es.client.dao.RunnerBoyLoginDaoInter;
import com.es.client.model.ResponseCodeModel;
import com.es.client.model.RunnerBoyInfoModel;
import com.es.client.model.UploadImageRequest;
import com.google.gson.Gson;

public class RBHandler implements RequestHandler<Map<String, String>, ResponseCodeModel>{
	
	enum RequestId{
		runnerBoyLogin, getCustomerList, updateDocuments 
	}
	
	private final static Gson gson = new Gson();

	@Override
	public ResponseCodeModel handleRequest(Map<String, String> map, Context context) {
		
		String requestType = map.get("requestId");
		System.out.println("Reuqest ID::"+requestType);
		
		RequestId requestId = RequestId.valueOf(requestType);
		String payLoad = map.get("payLoad");
		System.out.println("PayLoad :"+payLoad);
		ResponseCodeModel responseCodeModel = null;
		RunnerBoyLoginDaoInter runnerBoyLoginDaoInter = null;
		DocUploadDaoInter docUploadDaoInter = null;
		
		switch(requestId) {
			case runnerBoyLogin:
				runnerBoyLoginDaoInter = new RunnerBoyLoginDaoImpl();
				responseCodeModel = runnerBoyLoginDaoInter.runnerBoy(gson.fromJson(payLoad, RunnerBoyInfoModel.class));
				System.out.println(requestId);
				break;
			case getCustomerList:
				runnerBoyLoginDaoInter = new RunnerBoyLoginDaoImpl();
				responseCodeModel = runnerBoyLoginDaoInter.getCustomerList(gson.fromJson(payLoad, RunnerBoyInfoModel.class));
				System.out.println(">>>> Get Cust Final Result>>>>"+new Gson().toJson(responseCodeModel));
				System.out.println(requestId);
				break;
			case updateDocuments:
				docUploadDaoInter = new DocUploadDaoImp();
				responseCodeModel = docUploadDaoInter.updateDocImagePath(gson.fromJson(payLoad, UploadImageRequest.class));
				System.out.println(requestId);
				break;
			default:
				System.out.println("Unknown Request");
				break;
				
		
		}

		return responseCodeModel;
	}

}

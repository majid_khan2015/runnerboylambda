package com.es.client.service;
 
import com.es.client.dao.RunnerBoyLoginDaoImpl;
import com.es.client.model.ResponseCodeModel;
import com.es.client.model.RunnerBoyInfoModel;

public class RunnerBoyLoginserviceImpl implements RunnerBoyLoginserviceInter {

	RunnerBoyLoginDaoImpl runnerBoyDaoInter;
	
	@Override
	public ResponseCodeModel runnerBoy(RunnerBoyInfoModel runnerBoyInfoModel) {
		// TODO Auto-generated method stub
		return runnerBoyDaoInter.runnerBoy(runnerBoyInfoModel);
	}

	@Override
	public ResponseCodeModel getCustomerList(RunnerBoyInfoModel runnerBoyInfoModel) {
		// TODO Auto-generated method stub
		return runnerBoyDaoInter.getCustomerList(runnerBoyInfoModel);
	}

}

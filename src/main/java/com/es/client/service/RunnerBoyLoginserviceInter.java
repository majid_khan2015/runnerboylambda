/**
 * 
 */
package com.es.client.service;

import com.es.client.model.ResponseCodeModel;
import com.es.client.model.RunnerBoyInfoModel;

/**
 * @author majidkhan
 *
 */
public interface RunnerBoyLoginserviceInter {
	public ResponseCodeModel runnerBoy(RunnerBoyInfoModel runnerBoyInfoModel);
	public ResponseCodeModel getCustomerList(RunnerBoyInfoModel runnerBoyInfoModel);

}
